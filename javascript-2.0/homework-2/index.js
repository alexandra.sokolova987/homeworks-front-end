let root = document.querySelector("#root");
let list = document.createElement("ul");
root.append(list);

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

function MissData(message) {
    this.message = message;
    this.name = "MissData";
}
function noAuthor (){
    throw new MissData("There are no property author");
}
function noName (){
    throw new MissData("There are no property name");
}
function noPrice (){
    throw new MissData("There are no property price");
}

books.forEach((element, i) => {
    const {author, name, price} = element;
    let li = document.createElement("li");
    li.innerText = `Author: ${author},\nName: ${name},\nPrice: ${price}.`;
    list.append(li);

    try {
        if (!author) {
            noAuthor();
        } else if (!name) {
            noName();
        } else if (!price) {
            noPrice();
        } else {
            console.log("there are all properties");
        }
    } catch (error) {
        li.remove();
        console.log(error)
    }
});























