
function createEl(el) {
    let list = document.querySelector(".films");
    let li = document.createElement("li");
    li.innerHTML = `<span class = "bold">Title:</span> ${el.title}.<br> <span class = "bold">Episode</span> #${el.episode_id}.<br> <span class = "bold">Short description:</span> ${el.opening_crawl}`;
    list.appendChild(li);
    let ulCharacters = document.createElement("ul");
    ulCharacters.innerHTML = `<span class = "bold">Characters:</span> `;
    li.appendChild(ulCharacters);
    el.characters.forEach(item => {
            getCharacters(item, ulCharacters);
        }
    );
}

async function getFilms(url) {
    let response = await fetch(url);
    let json = await response.json();
    let results = await json.results;
    results.forEach(el => {
        createEl(el);
    });
    return results;
}

getFilms("https://swapi.dev/api/films");

async function getCharacters(url, ulCharacters) {
    let response = await fetch(url);
    let json = await response.json();
    let liCharacters = document.createElement("li");
    liCharacters.innerHTML = json.name;
    ulCharacters.appendChild(liCharacters);
}

getCharacters();




