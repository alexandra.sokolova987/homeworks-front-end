class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get getName(){
        console.log(`name is ${this.name}`);
    }
    get getAge(){
        console.log(`${this.name}'s age is ${this.age}`);
    }
    get getSalary(){
        console.log(`${this.name}'s salary is ${this.salary}`);
    }
    set setSalary(value){
        this.salary = value;
        console.log(`${this.name}'s salary is ${this.salary}`);
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get getSalary (){
        return this.salary * 3;
    }
}

let programmerIvan = new Programmer("Ivan", 25, 200, 3);
console.log(programmerIvan);

let programmerVasia = new Programmer("Vasia", 30, 400, 2);
let programmerMikle = new Programmer("Mikle", 37, 250, 1);
console.log(programmerMikle);
console.log(programmerVasia);