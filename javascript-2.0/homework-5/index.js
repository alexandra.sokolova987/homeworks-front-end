let ipButton = document.querySelector(".ip-btn");

function callAPI() {

    fetch("http://api.ipify.org/?format=json", {
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then((data) => {
            return data.json;
        })
        .then((data) => {
            fetch(`http://ip-api.com/${data.ip}`, {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                .then((response) => {
                    return response.json();
                })
                .then((data) => {
                    console.log(data);
                })
        });
}


ipButton.addEventListener("click", (e) => {
    callAPI();
});