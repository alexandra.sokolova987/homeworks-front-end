let button = document.querySelector(".header__burger-toggle");
let nav = document.querySelector(".nav");
let toggle = document.querySelector(".burger-toggle");

button.addEventListener("click", () => {
    nav.classList.toggle("nav_active");
    toggle.classList.toggle("burger-toggle_active");
});