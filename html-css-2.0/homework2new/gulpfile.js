const gulp = require("gulp");
const plumber = require("gulp-plumber");
const htmlValidator = require("gulp-w3c-html-validator");
const server = require("browser-sync").create();
const sass = require("gulp-sass");
const cleanCSS = require("gulp-clean-css");
const sourcemaps = require("gulp-sourcemaps");
const shorthand = require("gulp-shorthand");
const autoprefixer = require("gulp-autoprefixer");
const rename = require("gulp-rename");

const dev = gulp.parallel(html, styles);

module.exports.dev = gulp.series(dev, serve);