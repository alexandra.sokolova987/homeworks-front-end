//section our services

// function tab() {
//     let servicesTitle = document.querySelectorAll('.services-title');
//     let servicesItem = document.querySelectorAll('.list-content-item');
//     servicesTitle.forEach(item => {
//         item.addEventListener('click', selectTab)
//     });
//
//     function selectTab() {
//         servicesTitle.forEach(item => {
//             item.classList.remove('active-title');
//         });
//         this.classList.add('active-title');
//         let tabName;
//         tabName = this.getAttribute('data-name');
//         selectTabContent(tabName);
//     }
//
//     function selectTabContent(tabName) {
//         servicesItem.forEach(item => {
//             if (item.classList.contains(tabName)) {
//                 item.classList.add('active-item');
//             } else {
//                 item.classList.remove('active-item');
//             }
//         })
//     }
// }
//
// tab();


let servicesList = document.querySelector('.services-list');
let servicesTitle = document.querySelectorAll('.services-title');
let servicesItem = document.querySelectorAll('.list-content-item');

servicesList.addEventListener('click', (e) => {
    let tg = e.target;
    servicesTitle.forEach((elem) => {
        elem.classList.remove("active-title")
    });

    if(tg.classList.contains("services-title")){
        servicesTitle.forEach((tab, i) => {
            if (tg === tab) {
                servicesItem.forEach((item) => {
                    item.style.display = 'none';
                });
                tab.classList.add("active-title");
                servicesItem.forEach((item, i) => {
                    if (tg.innerText === servicesItem[i].id) {
                        servicesItem[i].style.display = 'flex';
                    }
                })
            }
        })
    }
});

// section our amazing work
// svg hover

let svgHover = document.querySelectorAll('.amazing-work-circle-1');
svgHover.forEach(item => {
    item.innerHTML = `<svg width="15" height="15" viewBox="0 0 15 15" fill="#fff" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z" fill="#1FDAB5"/>
</svg>`
    item.classList.add('svg-hover')
});

svgHover.forEach(item => {
    item.addEventListener('mouseover', (event) => {
        item.innerHTML = `<svg width="15" height="15" viewBox="0 0 15 15" fill="#fff" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z" fill="#fff"/>
</svg>`
    })
    item.addEventListener('mouseout', (event) => {
        item.innerHTML = `<svg width="15" height="15" viewBox="0 0 15 15" fill="#fff" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z" fill="#1FDAB5"/>
</svg>`
    })
});



// load button
let loading = document.createElement('div');
loading.insertAdjacentHTML('afterbegin', `<?xml version="1.0" encoding="utf-8"?>
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin: auto; background: rgb(241, 242, 243); display: block; shape-rendering: auto;" width="38px" height="38px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
<g transform="translate(67,50)">
<g transform="rotate(0)">
<circle cx="0" cy="0" r="6" fill="#93dbe9" fill-opacity="1">
  <animateTransform attributeName="transform" type="scale" begin="-1.6339869281045751s" values="1.5 1.5;1 1" keyTimes="0;1" dur="1.9607843137254901s" repeatCount="indefinite"></animateTransform>
  <animate attributeName="fill-opacity" keyTimes="0;1" dur="1.9607843137254901s" repeatCount="indefinite" values="1;0" begin="-1.6339869281045751s"></animate>
</circle>
</g>
</g><g transform="translate(58.5,64.72243186433546)">
<g transform="rotate(59.99999999999999)">
<circle cx="0" cy="0" r="6" fill="#93dbe9" fill-opacity="0.8333333333333334">
  <animateTransform attributeName="transform" type="scale" begin="-1.3071895424836601s" values="1.5 1.5;1 1" keyTimes="0;1" dur="1.9607843137254901s" repeatCount="indefinite"></animateTransform>
  <animate attributeName="fill-opacity" keyTimes="0;1" dur="1.9607843137254901s" repeatCount="indefinite" values="1;0" begin="-1.3071895424836601s"></animate>
</circle>
</g>
</g><g transform="translate(41.5,64.72243186433546)">
<g transform="rotate(119.99999999999999)">
<circle cx="0" cy="0" r="6" fill="#93dbe9" fill-opacity="0.6666666666666666">
  <animateTransform attributeName="transform" type="scale" begin="-0.9803921568627451s" values="1.5 1.5;1 1" keyTimes="0;1" dur="1.9607843137254901s" repeatCount="indefinite"></animateTransform>
  <animate attributeName="fill-opacity" keyTimes="0;1" dur="1.9607843137254901s" repeatCount="indefinite" values="1;0" begin="-0.9803921568627451s"></animate>
</circle>
</g>
</g><g transform="translate(33,50)">
<g transform="rotate(180)">
<circle cx="0" cy="0" r="6" fill="#93dbe9" fill-opacity="0.5">
  <animateTransform attributeName="transform" type="scale" begin="-0.6535947712418301s" values="1.5 1.5;1 1" keyTimes="0;1" dur="1.9607843137254901s" repeatCount="indefinite"></animateTransform>
  <animate attributeName="fill-opacity" keyTimes="0;1" dur="1.9607843137254901s" repeatCount="indefinite" values="1;0" begin="-0.6535947712418301s"></animate>
</circle>
</g>
</g><g transform="translate(41.49999999999999,35.277568135664545)">
<g transform="rotate(239.99999999999997)">
<circle cx="0" cy="0" r="6" fill="#93dbe9" fill-opacity="0.3333333333333333">
  <animateTransform attributeName="transform" type="scale" begin="-0.32679738562091504s" values="1.5 1.5;1 1" keyTimes="0;1" dur="1.9607843137254901s" repeatCount="indefinite"></animateTransform>
  <animate attributeName="fill-opacity" keyTimes="0;1" dur="1.9607843137254901s" repeatCount="indefinite" values="1;0" begin="-0.32679738562091504s"></animate>
</circle>
</g>
</g><g transform="translate(58.5,35.277568135664545)">
<g transform="rotate(300.00000000000006)">
<circle cx="0" cy="0" r="6" fill="#93dbe9" fill-opacity="0.16666666666666666">
  <animateTransform attributeName="transform" type="scale" begin="0s" values="1.5 1.5;1 1" keyTimes="0;1" dur="1.9607843137254901s" repeatCount="indefinite"></animateTransform>
  <animate attributeName="fill-opacity" keyTimes="0;1" dur="1.9607843137254901s" repeatCount="indefinite" values="1;0" begin="0s"></animate>
</circle>
</g>
</g>`);
loading.classList.add('loading-hidden');
document.querySelector('.img-open').appendChild(loading);

let loadMore = document.querySelector('.load-more');
let imgClosed = document.querySelectorAll('.img-closed-item');

function loadButton() {
    if (loading.classList.contains('loading')) {
        loading.classList.remove('loading');
        loading.classList.add('loading-hidden');
    }
    imgClosed.forEach(item => {
        if (!item.classList.contains('img-closed-item-visible')) {
            item.classList.add('img-closed-item-visible');
        }
    });

}

loadMore.addEventListener('click', () => {
    loadMore.classList.add('load-more-hidden');
    loading.classList.add('loading');
    setTimeout(loadButton, 3000);
});

// amazing works tabs
// function amazingTubs() {
//     let amazingWorkTitle = document.querySelectorAll('.amazing-work-title');
//     let amazingWorkItem = document.querySelectorAll('.all');
//     amazingWorkTitle.forEach(item => {
//         item.addEventListener('click', SelectAmazingTubs);
//     });
//
//     function SelectAmazingTubs() {
//         amazingWorkTitle.forEach(item => {
//             item.classList.remove('amazing-active-title');
//         });
//         this.classList.add('amazing-active-title');
//         let tabName;
//         tabName = this.getAttribute('data-name');
//         SelectAmazingContent(tabName);
//
//     }
//     function SelectAmazingContent(tabName) {
//         amazingWorkItem.forEach((item, i) => {
//             if(!item.classList.contains(tabName)){
//                 item.classList.remove('active');
//             } else {
//                 item.classList.add('active');
//             }
//         })
//     }
// }
// amazingTubs();


let workList = document.querySelector('.amazing-work-list');
let workTitle = document.querySelectorAll('.amazing-work-title');
let workItem = document.querySelectorAll('.all');

workList.addEventListener('click', (e) => {
    let tg = e.target;
    workTitle.forEach((elem) => {
        elem.classList.remove("amazing-active-title")
    });

    if (tg.classList.contains("amazing-work-title")) {
        workTitle.forEach((tab, i) => {
            if (tg === tab) {
                workItem.forEach((item) => {
                    item.style.display = 'none';
                });
                tab.classList.add("amazing-active-title");
                workItem.forEach((item, i) => {
                    if (tg.innerText === workItem[i].dataset.name) {
                        workItem[i].style.display = 'flex';
                    }
                    if (tg.innerText === workItem[i].dataset.all) {
                        workItem[i].style.display = 'flex';
                    }
                })
            }
        })
    }
});












// Glide
new Glide('.glide', {
    type: 'carousel',
}).mount();


