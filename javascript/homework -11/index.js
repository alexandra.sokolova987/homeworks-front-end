let button = document.querySelectorAll('.btn');

document.addEventListener('keydown', (e) => {
    if(e.code === "Enter"){
        buttonColors(0);
    } else if(e.code === "KeyS"){
        buttonColors(1);
    } else if(e.code === "KeyE"){
        buttonColors(2);
    } else if(e.code === "KeyO"){
        buttonColors(3);
    } else if(e.code === "KeyN"){
        buttonColors(4);
    } else if(e.code === "KeyL"){
        buttonColors(5);
    } else if(e.code === "KeyZ"){
        buttonColors(6);
    }
});
function buttonColors (i){
    button.forEach(item => {
        if(item.classList.contains('btn-blue-color')){
            item.classList.remove('btn-blue-color');
            item.classList.add('btn-black-color');
        }
    });
    button[i].classList.remove('btn-black-color');
    button[i].classList.add('btn-blue-color');
}