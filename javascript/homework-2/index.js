let endGap = +prompt ("Введите второе значение для промежутка", "");

if (endGap > 5 || endGap < -5) {
    if (endGap > 0) {
        for (let i = 0; i <= endGap; i++) {
            if (i % 5 === 0 && i !== 0) {
                console.log(i);
            }
        }
    } else if (endGap < 0){
        for (let j = 0; j >= endGap; j--) {
            if (j % 5 === 0 && j !== 0) {
                console.log(j);
            }
        }
    }
} else {
    alert("Sorry, no numbers");
}