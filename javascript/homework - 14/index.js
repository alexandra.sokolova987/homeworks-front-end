
// $('.btn-nav').click(function(e){
//     switch (e.target.innerText) {
//         case "MOST POPULAR POSTS":
//             $('body,html').animate({scrollTop: $('btn-nav').offset().top}, 1500);
//             break;
//         case "OUR MOST POPULAR CLIENTS":
//             $('body,html').animate({scrollTop: $('btn-nav').offset().top}, 1500);
//             break;
//         case "TOP RATED":
//             $('body,html').animate({scrollTop: $('btn-nav').offset().top}, 1500);
//             break;
//         case "HOT NEWS":
//             $('body,html').animate({scrollTop: $('btn-nav').offset().top},  1500);
//             break;
//
//     }
// });

// переход к нужному разделу по клику на меню
$("#menu").on("click","a", function (event) {
    let id  = $(this).attr('href');
    let top = $(id).offset().top;
    $('body,html').animate({scrollTop: top}, 1500);
});

// появление кнопки при прокрутке более чем на 1 экран
$(window).scroll(function () {
    if ($(this).scrollTop() > 500) {
        $('.scroll-up-btn').css('opacity','1');
    } else {
        $('.scroll-up-btn').css('opacity','0');
    }
});
// при клике по кнопке скролл наверх
$('.scroll-up-btn').click(function () {
    $("html, body").animate({scrollTop: 0}, 1000);
});

// показать\спрятать блок
$('.show-or-hide-btn').click(() => {
    $('.top-rated-images').slideToggle();
});