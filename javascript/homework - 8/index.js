let input = document.querySelector('.margin-for-input');
let form = document.querySelector('.form-for-price');

let div = document.createElement('div');
document.body.prepend(div);
let error = document.createElement('div');
form.after(error);
let textPrice;
let btn;
let errorMessage;
let errorBtn;
input.addEventListener('focusin', () => {
    if (input.value > 0) {
        textPrice.hidden = true;
        btn.hidden = true;
        div.hidden = true;
        div.classList.remove('current-price');
        input.value = "";

    } else if (input.value < 0 || isNaN(input.valueAsNumber)) {
        errorMessage.hidden = true;
        errorBtn.hidden = true;
        error.hidden = true;
        error.classList.remove('error');
        input.classList.remove('red-border');
        input.value = "";
    } else {
        input.classList.add('green-border');
    }

});
input.addEventListener('focusout', () => {
    if (input.value <= 0) {
        input.classList.remove('green-border');
        input.classList.add('red-border');
        error.classList.add('error');
        errorMessage = document.createElement('p');
        errorMessage.innerText = 'Please enter correct price';
        error.prepend(errorMessage);
        errorBtn = document.createElement('button');
        errorBtn.innerHTML = `&#10008`;
        errorMessage.after(errorBtn);
        errorBtn.addEventListener('click', () => {
            errorMessage.hidden = true;
            errorBtn.hidden = true;
            error.hidden = true;
            error.classList.remove('error');
            input.classList.remove('red-border');
            input.value = "";
        });
    } else {
        textPrice = document.createElement('span');
        textPrice.innerHTML = `Текущая цена: &#36 ${input.value} `;
        btn = document.createElement('button');
        btn.innerHTML = `&#10008`;
        div.appendChild(textPrice);
        div.appendChild(btn);
        div.classList.add('current-price');
        btn.addEventListener('click', () => {
            textPrice.hidden = true;
            btn.hidden = true;
            div.hidden = true;
            div.classList.remove('current-price');
            input.classList.remove('green-border');
            input.value = "";
        });
    }
});





