function filterBy(arr, a) {
    let val;
    val = arr.filter((elem) => {
        return typeof elem !== a;
    });
    return val;
}
let arr = ['hello', 'world', 23, '23', null];
let value = filterBy(arr, "string");
console.log(value);