let changeTheme = document.querySelector('.change-theme');

let pricingTable = document.querySelector('.pricing-table');
let choosePack = document.querySelectorAll('.choose-pack');
let greenText = document.querySelectorAll('.green-text');
let redText = document.querySelectorAll('.red-text');
let blueText = document.querySelectorAll('.blue-text');
let buttonShadowGreen = document.querySelector('.button-shadow-green');
let buttonShadowRed = document.querySelector('.button-shadow-red');
let buttonShadowBlue = document.querySelector('.button-shadow-blue');
let footButton = document.querySelectorAll('.foot-button');

changeTheme.addEventListener('click', () => {
    if(pricingTable.classList.contains('pricing-table-2d-theme')){
        pricingTable.classList.remove('pricing-table-2d-theme');
    } else {
        pricingTable.classList.add('pricing-table-2d-theme');
    }
    if(buttonShadowGreen.classList.contains('button-shadow-green-2d-theme')){
        buttonShadowGreen.classList.remove('button-shadow-green-2d-theme');
    } else {
        buttonShadowGreen.classList.add('button-shadow-green-2d-theme');
    }
    if(buttonShadowRed.classList.contains('button-shadow-red-2d-theme')){
        buttonShadowRed.classList.remove('button-shadow-red-2d-theme');
    } else {
        buttonShadowRed.classList.add('button-shadow-red-2d-theme');
    }
    if(buttonShadowBlue.classList.contains('button-shadow-blue-2d-theme')){
        buttonShadowBlue.classList.remove('button-shadow-blue-2d-theme');
    } else {
        buttonShadowBlue.classList.add('button-shadow-blue-2d-theme');
    }
    choosePack.forEach(item => {
        if(item.classList.contains('choose-pack-2d-theme')){
            item.classList.remove('choose-pack-2d-theme');
        } else {
            item.classList.add('choose-pack-2d-theme');
        }
    });
    greenText.forEach(item => {
        if(item.classList.contains('green-text-2d-theme')){
            item.classList.remove('green-text-2d-theme');
        } else {
            item.classList.add('green-text-2d-theme');
        }
    });
    redText.forEach(item => {
        if(item.classList.contains('red-text-2d-theme')){
            item.classList.remove('red-text-2d-theme');
        } else {
            item.classList.add('red-text-2d-theme');
        }
    });
    blueText.forEach(item => {
        if(item.classList.contains('blue-text-2d-theme')){
            item.classList.remove('blue-text-2d-theme');
        } else {
            item.classList.add('blue-text-2d-theme');
        }
    });
    footButton.forEach(item => {
        if(item.classList.contains('foot-button-2d-theme')){
            item.classList.remove('foot-button-2d-theme');
        } else {
            item.classList.add('foot-button-2d-theme');
        }
    });
});
