// функция подсчета факториала числа

let num = +prompt("Enter a number", "");          // пользователь вводит число
if (num === "" || isNaN(num)){                                     // проверка числа на NaN и на пустую строку, если пользователь ничего не ввел
    num = +prompt("Enter a number again", "");
}

function factorial (num){                                          // фунция подсчета факториала
    let res = 1;
    for(let i = 1; i <= num; i++){
        res*=i;
    }
    return res;
}

console.log(factorial(num));                                       // вызов ф-ции