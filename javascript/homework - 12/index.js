let imgItem = document.querySelectorAll('.img-item');
let btnStop = document.querySelector('.btn-stop');
let btnContinue = document.querySelector('.btn-continue');

function imgChange() {
    let index = null;
    imgItem.forEach((item, i) => {
        if (item.classList.contains('active')) {
            index = i;
        }
    });
    if (index !== imgItem.length -1) {
        imgItem[index].classList.remove('active');
        imgItem[index + 1].classList.add('active');
    } else {
        imgItem[index].classList.remove('active');
        imgItem[0].classList.add('active');
    }
}
let interval = setInterval(imgChange, 2000);;
// btnStart.addEventListener('click', ()=> {
//     interval = setInterval(imgChange, 2000);
// });



btnStop.addEventListener('click', (e) => {
    e.preventDefault();
    clearInterval(interval);
    // setTimeout(() => { clearInterval(interval) });
});
btnContinue.addEventListener('click', (e)=> {
    e.preventDefault();
    interval = setInterval(imgChange, 2000);
});