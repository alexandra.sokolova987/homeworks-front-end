let eye = document.querySelectorAll('.fas');
let input = document.querySelectorAll('.enter-password');
let btn = document.querySelector('.btn');
let error = document.createElement('span');
btn.after(error);
error.innerText = "You must enter the same values";
error.classList.add('error-hidden');
let popup = document.querySelector('.popup-wrapper');
let popupClose = document.querySelector('.popup-close');

eye.forEach((item, i) => {
    item.addEventListener('click', () => {
        if(!item.classList.contains('fa-eye-slash')){
            item.classList.remove('fa-eye');
            item.classList.add('fa-eye-slash');
            input[i].setAttribute('type', 'text')
        } else {
            item.classList.remove('fa-eye-slash');
            item.classList.add('fa-eye');
            input[i].setAttribute('type', 'password');
        }
    })
});

btn.addEventListener('click', (e) => {
    e.preventDefault();
    if(input[0].value === input[1].value && input[0].value !== "" && input[1].value !== ""){
        popup.classList.add('popup-wrapper-active');
    } else {
        error.classList.add('error-active');
        input.forEach(item => {
            item.value = "";
        })
    }
});
popupClose.addEventListener('click', () => {
    popup.classList.remove('popup-wrapper-active');
    input.forEach(item => {
        item.value = "";
    })
});

input.forEach(item => {
    item.addEventListener('click', () => {
        error.classList.remove('error-active');
    })
});
