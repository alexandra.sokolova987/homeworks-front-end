function createNewUser (){
    let name = prompt("What is your name?", "");
    let lastName = prompt("What is your last name?", "");
    let birthday = prompt("What is your date of birth? (dd.mm.yyyy)", "");
    let now = new Date().toLocaleDateString();
    let newUser;
    return newUser = {           // создание объекта
        firstName: name,
        lastName,
        birthday,
        getLogin: function(){     // ф-ция, которая возвращает первую букву имени и фамилию
            return `${newUser.firstName.toLowerCase().substr(0,1)}` + `${newUser.lastName.toLowerCase()}`
        },
        getCurrentAge: function(){     // ф-ция, которая считает и возвращает кол-во полных лет
            let age;
            if(newUser.birthday.slice(3,5) < now.slice(3,5)) {
                age = new Date().getFullYear() - birthday.slice(-4);
            } else if (newUser.birthday.slice(3,5) > now.slice(3,5)){
                age = new Date().getFullYear() - birthday.slice(-4) - 1;
            } else {
                if(newUser.birthday.slice(0,2) < now.slice(0,2)){
                    age = new Date().getFullYear() - birthday.slice(-4);
                } else {
                    age = new Date().getFullYear() - birthday.slice(-4) - 1;
                }
            }
            return age;
        },
        getPassword: function(){      // ф-ция, которая возвращает первую букву имени + фамилию + год рождения
            return `${newUser.firstName.toUpperCase().substr(0,1)}` + `${newUser.lastName.toLowerCase()}` + `${newUser.birthday.slice(-4)}`
        }
    }
};

// console.log(createNewUser().getLogin());
console.log(createNewUser().getCurrentAge());
// console.log(createNewUser().getPassword());

























