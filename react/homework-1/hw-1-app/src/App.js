import React, {Component} from 'react';
import Modal from "./components/Modal";
import Button from "./components/Button";
import './App.scss';

class App extends Component {
  state = {
    isOpen1: false,
    isOpen2: false,
  }
  toggleModal1 = () => {
    const modalState = this.state.isOpen1;
    this.setState({isOpen1: !modalState}) 
  }
  toggleModal2 = () => {
    const modalState = this.state.isOpen2;
    this.setState({isOpen2: !modalState}) 
  }
   
  render() {
    return (
      <div>
        <Button text={"Open first modal"} backgroundColor={'MediumSlateBlue'} onClick = {this.toggleModal1} />
        <Button text={"Open second modal"} backgroundColor={"SandyBrown"} onClick = {this.toggleModal2}/>

        <Modal title={"Do you want to delete this file?"} 
        text={"Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?"} 
        isOpen={this.state.isOpen1} 
        closeButton={true} 
        closeClick = {this.toggleModal1}
        actions={<div className="foot_btn">
                  <button className="foot_btn_ok" onClick={this.toggleModal1}>Ok</button>
                  <button className="foot_btn_close" onClick={this.toggleModal1}>Close</button>
                </div>}/>
        <Modal title={"second modal"} 
        text={"Second modal text"} 
        isOpen={this.state.isOpen2} 
        closeButton={false} 
        closeClick = {this.toggleModal2}
        actions={<div className="foot_btn">
                  <button className="foot_btn_ok" onClick={this.toggleModal2}>Send</button>
                  <button className="foot_btn_close" onClick={this.toggleModal2}>Cancel</button>
                </div>}/>
        
      </div>
    )
  }
}


export default App;
