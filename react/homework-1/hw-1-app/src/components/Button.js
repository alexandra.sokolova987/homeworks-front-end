import React, { Component } from "react";
import Modal from "./Modal";


class Button extends Component {
  render(){
    const {text, style, onClick, backgroundColor} = this.props;
    return (
    <button className="btn" style={{backgroundColor}} onClick={onClick}>{text}</button>
    )
  }
}

export default Button;