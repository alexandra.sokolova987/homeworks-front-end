import React, { Component } from "react";
import Modal from "../Modal/Modal";
import PropTypes from 'prop-types';



class Button extends Component {
  render(){
    const {text, onClick, backgroundColor} = this.props;
    return (
    <button className="btn" style={{backgroundColor}} onClick={onClick}>{text}</button>
    )
  }
}

Button.propTypes = {
  text: PropTypes.string,
  onClick: PropTypes.func,
  backgroundColor: PropTypes.string
}
Button.defaultProps = {
  text: 'Click me to add this to cart',
  onClick: null,
  backgroundColor: 'Black'
}

export default Button;