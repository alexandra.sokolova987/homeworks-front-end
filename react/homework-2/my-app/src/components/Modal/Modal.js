import React, { Component, Fragment } from "react";
import Button from "../Button/Button";
import PropTypes from "prop-types";

class Modal extends Component {
  render() {
    const {
      title,
      text,
      isOpen,
      closeButton,
      actions,
      closeClick,
    } = this.props;

    const onModalClick = (e) => {
      e.stopPropagation();
    };

    return (
      <>
        {isOpen && (
          <div className="modal" onClick={closeClick}>
            <div className="modal_body" onClick={onModalClick}>
              <div className="modal_head">
                <h1 className="modal_title">{title}</h1>
                {closeButton && (
                  <button className="modal_btn_close" onClick={closeClick}>
                    &times;
                  </button>
                )}
              </div>
              <div className="modal_content">
                <p className="modal_text">{text}</p>
                {actions}
              </div>
            </div>
          </div>
        )}
      </>
    );
  }
}

Modal.propTypes = {
  title: PropTypes.string,
  text: PropTypes.string,
  isOpen: PropTypes.bool.isRequired,
  closeButton: PropTypes.bool.isRequired,
  closeClick: PropTypes.func
}

Modal.defaultProps = {
  title: 'Title of modal',
  text: 'text of modal',
  closeClick: null
}

export default Modal;
