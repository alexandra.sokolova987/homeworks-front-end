import React, { Component } from "react";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import PropTypes from "prop-types";
import AddToCart from "./Cart/AddToCart";
import AddToFavorites from "./Favorites/AddToFavorites";
import "./Card.scss";

class Card extends Component {
  render() {
    const { cards } = this.props;
    console.log({ cards });

    return (
      <div className="card">
        <div>Name: {cards.name}</div>
        <div>
          Color:{" "}
          <div
            className="color_wrapper"
            style={{ backgroundColor: cards.color }}
          ></div>
        </div>
        <div>Price: {cards.price} $</div>
        <img src={cards.path} className="card_img"></img>
        <div className="add_card">
          <AddToCart></AddToCart>
          <AddToFavorites></AddToFavorites>
        </div>
      </div>
    );
  }
}

{
  /* Card.propTypes = {
  cards: PropTypes.object.isRequired
} */
}

export default Card;
