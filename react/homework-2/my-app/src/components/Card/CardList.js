import React, { Component, Fragment } from 'react';
import Card from './Card';
import PropTypes from 'prop-types';



class CardList extends Component {
    render(){
        const { cards } = this.props;

        const cardsGood = cards.map(e => <Card key={cards.id} cards={e} />);

        return (
            <div className='card_wrapper'>
                {cardsGood}
            </div>
        )
    }
};

CardList.propTypes = {
    cards: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.string,
        price: PropTypes.number,
        color: PropTypes.string
    })).isRequired
  }

export default CardList;