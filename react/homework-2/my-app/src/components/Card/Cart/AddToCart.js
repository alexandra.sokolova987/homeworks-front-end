import React, { Component, Fragment } from "react";
import Button from "../../Button/Button";
import Modal from "../../Modal/Modal";

class AddToCart extends Component {
  state = {
    isOpen1: false,
    isOpen2: false,
    cards: [],
  };
  toggleModal1 = () => {
    const modalState = this.state.isOpen1;
    this.setState({ isOpen1: !modalState });
  };
  toggleModal2 = () => {
    const modalState = this.state.isOpen2;
    this.setState({ isOpen2: !modalState });
  };
  render() {
    return (
      <>
        <Button
          text={"Add to cart"}
          backgroundColor={"Brown"}
          onClick={this.toggleModal1}
        />
        <Modal
          title={"Do you want to delete this file?"}
          text={
            "Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?"
          }
          isOpen={this.state.isOpen1}
          closeButton={true}
          closeClick={this.toggleModal1}
          actions={
            <div className="foot_btn">
              <button className="foot_btn_ok" onClick={this.toggleModal1}>
                Ok
              </button>
              <button className="foot_btn_close" onClick={this.toggleModal1}>
                Close
              </button>
            </div>
          }
        />
      </>
    );
  }
}

export default AddToCart;
