import React, { Component } from 'react';
import './Cart.scss';

class Cart extends Component {
  state = {
    cart: []
  }
  render(){
    const { cart } = this.state;
    return(
    <h3 className='cart'>Cart: {cart.length}</h3>
    )
  }

}

export default Cart;