import React, {Component} from 'react';
import Modal from "./components/Modal/Modal";
import Button from "./components/Button/Button";
import CardList from "./components/Card/CardList";
import Cart from './components/Card/Cart/Cart';
import axios from 'axios';
import './App.scss';


class App extends Component {
  state = {
      cards: []
  }
 
  componentDidMount(){
      axios('/cards.json')
          .then(res => {
              this.setState({ cards: res.data})
          })
  }

  render() {
      const {cards} = this.state;

    return (
      <div>
        <Cart></Cart>
        <CardList cards = {cards}></CardList>
    
        {/* <Button text={"Open second modal"} backgroundColor={"SandyBrown"} onClick = {this.toggleModal2}/> */}

        {/* <Modal title={"second modal"} 
        text={"Second modal text"} 
        isOpen={this.state.isOpen2} 
        closeButton={false} 
        closeClick = {this.toggleModal2}
        actions={<div className="foot_btn">
                  <button className="foot_btn_ok" onClick={this.toggleModal2}>Send</button>
                  <button className="foot_btn_close" onClick={this.toggleModal2}>Cancel</button>
                </div>}/> */}
        
      </div>
    )
  }
}


export default App;
